module RailsAdminContentBlocks
  class Engine < ::Rails::Engine

    initializer "RailsAdminContentBlocks precompile hook", group: :all do |app|

      app.config.assets.precompile += %w(rails_admin/rails_admin_content_blocks.js rails_admin/rails_admin_content_blocks.css)
    end
    #
    initializer 'Include RailsAdminContentBlocks::Helper' do |app|
      ActionView::Base.send :include, RailsAdminContentBlocks::Helper
    end
  end
end
