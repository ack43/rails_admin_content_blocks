module RailsAdminContentBlocks
  class Configuration
    def initialize(abstract_model)
      @abstract_model = abstract_model
    end

    def options
      @options ||= {
          blocks: {},
          path: @abstract_model.model.name.underscore,
          base_block: "base"
      }.merge(config || {})
    end

    protected
    def config
      ::RailsAdmin::Config.model(@abstract_model.model).content_blocks || {}
    end
  end
end
