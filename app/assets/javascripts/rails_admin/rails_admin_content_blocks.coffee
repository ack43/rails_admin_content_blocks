#= require medium-editor

$(document).ready ->

  window.nestedFormEvents.oldInsertFields = window.nestedFormEvents.insertFields
  window.nestedFormEvents.insertFields = (content, assoc, link) ->
    if $(link).is(".add-content-block")
      target = $(link).data('target')
      content = $(content)
      if target
        content.appendTo $(target)
      else
        content.insertBefore($(link).closest('.content-block'))
      content.highlight("new")
      content.addClass('content-block')
      window.railsAdminContentBlocks.initDropzone()
      window.railsAdminContentBlocks.initMediumEditor()
    else
      window.nestedFormEvents.oldInsertFields(content, assoc, link)


unless window.railsAdminContentBlocks
  window.railsAdminContentBlocks = {}

  # lists
  $(document).on "change", ".list-type-changer", (e)->
    me = $(e.currentTarget)
    block = me.closest(".block.block-list")
    if block.length > 0
      list_content = block.find(".list-content")
      list_content.addClass("hidden").filter(me.find(":selected").data('target')).removeClass('hidden')


  # Dropzone
  window.railsAdminContentBlocks.initDropzone = ()->
    new_dz = $(".block.block-image:not(.dropzone-initiated)")
    baseForm = $("#block_image_prototype")
    new_dz.each ->
      block = $(this)
      field = block.find("input[type='file'][data-image-file]")
      prototype_field = block.find("input[type='hidden'][data-image-prototype]")
      dz = new Dropzone(this, {
        url: baseForm.attr('action'),
        paramName: baseForm.find('input[type="file"]').attr("name"),
        params: { authenticity_token: baseForm.find("[name='authenticity_token']").val(), format: "js" },
        withCredentials: true,
        ##
        autoProcessQueue: false,
        addRemoveLinks:   true,
        parallelUploads: 1,
        maxFiles: 1
        ##
      })
      dz.on "complete", (file)->
        prototype = JSON.parse(file.xhr.response) if file.xhr and file.xhr.response
        if prototype
          prototype_field.val(prototype.id)
          field.val("")
      block.data('dz', dz)
      block.data('field', field)
      block.data('prototype_field', prototype_field)
      block.addClass('dropzone-initiated')
      block.after($("<button>").addClass("hidden").text("загрузить").click((e)->
        e.preventDefault()
        $(e.currentTarget).prev()[0].dropzone.processQueue()
        return false
      ))



  # highlight
  window.$.fn.highlight = (type = 'update', callback = (highlight_target)->)->
    highlight_target = $(this)
    if highlight_target.length > 0
      if type is 'function'
        callback = type
        type = 'update'
      highlight_target.addClass('highlight').addClass("highlight-#{type}")
      setTimeout ->
        highlight_target.removeClass('highlight').removeClass("highlight-#{type}")
        callback(highlight_target) if callback
      , 1000
    highlight_target

  window.highlight_for = (highlight_target, type = 'update', callback = (highlight_target)->)->
    $(highlight_target).highlight(type, callback)
  ################################################

  # recalculateSortableBlocks
  window.railsAdminContentBlocks.recalculateSortableBlocks = ()->
    counter = 0
    $("#drag-content-blocks > .content-block > span.content").each ->
      this.innerText = counter++

  #recalculateBlockOrders
  window.railsAdminContentBlocks.recalculateBlockOrders = ()->
    counter = 0
    $("#drag-content-blocks > .content-block > span.content input.order-field").each ->
      this.value = counter++
  window.railsAdminContentBlocks.recalculateBlockOrders() # initial order
  ################################################


  # $(document).delegate "click", "#rails_admin_content_blocks_btn", (e)->
  #   alert('click')


  # add/remove li
  # $(document).on 'click', '#drag-content-blocks ~ .add-bottom-li', (e) ->
  #   ul = $("#drag-content-blocks")
  #   template = $(ul.data('template'))
  #   ul.append(template.highlight("new"))
  #   window.railsAdminContentBlocks.recalculateSortableBlocks()

  # $(document).on 'click', '#drag-content-blocks > .content-block > span.add', (e) ->
  #   nextLi = $(e.currentTarget.parentNode)
  #   template = $($("#drag-content-blocks").data('template'))
  #   nextLi.before(template.highlight("new"))
  #   window.railsAdminContentBlocks.recalculateSortableBlocks()

  # $(document).on 'click', '#drag-content-blocks > .content-block > span.remove', (e) ->
  #   $(e.currentTarget.parentNode).remove()
  ################################################


  $(document).on "change", "#content_blocks select.content-block-type-changer", (e)->
    select = $(e.currentTarget)
    fields = select.closest(".fields")
    fields.find(".block").addClass('hidden').filter(".block-#{select.val()}").removeClass('hidden')



  # init sortableBlocks (drop operations)
  window.railsAdminContentBlocks.sortableBlocks = ->

    $(document).on 'dragstart', '#drag-content-blocks > .content-block > span.drag', (e) ->
      $(e.currentTarget.parentNode).addClass("selected")
      $("#drag-content-blocks").data('drag-data', e.currentTarget.parentNode)

    $(document).on 'dragover', '#drag-content-blocks > .content-block, #drag-content-blocks > .content-block *', (e) ->
      e.preventDefault()
      content_block = $(e.currentTarget)
      unless content_block.is(".content-block")
        content_block = content_block.closest(".content-block")
      content_block = content_block[0]
      if content_block
        # target = e.currentTarget
        # content_block = target.parentNode
        bounding = content_block.getBoundingClientRect()
        offset = bounding.y + bounding.height / 2
        if e.clientY - offset > 0
          content_block.style['border-bottom'] = 'solid 4px blue'
          content_block.style['border-top'] = ''
        else
          content_block.style['border-top'] = 'solid 4px blue'
          content_block.style['border-bottom'] = ''

    $(document).on 'dragleave', '#drag-content-blocks > .content-block, #drag-content-blocks > .content-block *', (e) ->
      content_block = $(e.currentTarget)
      unless content_block.is(".content-block")
        content_block = content_block.closest(".content-block")
      content_block = content_block[0]
      if content_block
        # target = e.currentTarget
        # content_block = target.parentNode
        content_block.style['border-bottom'] = ''
        content_block.style['border-top'] = ''

    # $(document).on 'drop', '#drag-content-blocks > .content-block, #drag-content-blocks > .content-block *', (e) ->
    $(document).on 'drop', '*', (e) ->
      console.log('drop')
      console.log(e.currentTarget)
      data = $("#drag-content-blocks").data('drag-data')
      $("#drag-content-blocks").data('drag-data', null)
      return unless data
      e.preventDefault()
      content_block = $(e.currentTarget)
      unless content_block.is(".content-block")
        content_block = content_block.closest(".content-block")
        content_block = [] unless content_block.parent().is("#drag-content-blocks")
      content_block = content_block[0]
      if content_block
        # target = e.currentTarget
        # content_block = target.parentNode
        ul = content_block.parentNode
        if content_block.style['border-bottom'] != ''
          content_block.style['border-bottom'] = ''
          ul.insertBefore data, content_block.nextSibling
        else
          content_block.style['border-top'] = ''
          ul.insertBefore data, content_block
        $(ul).find(".content-block.selected").removeClass("selected").highlight("replace")
        window.railsAdminContentBlocks.recalculateBlockOrders()
      else
        $("#drag-content-blocks .content-block.selected").removeClass("selected")
      e.stopPropagation()


  window.railsAdminContentBlocks.sortableBlocks()
  ################################################

  window.railsAdminContentBlocks.initMediumEditor = ()->
    editable = $(".medium-editor-block:not(.medium-editor-initiated)")
    editable.each ->
      ed = $(this)
      # textarea = ed.prev()
      editor = new MediumEditor(this, {})
      # editor = new MediumEditor(this, {
      #   toolbar: {
      #     buttons: [
      #       {
      #         name: 'h4',
      #         action: 'append-h4',
      #         aria: 'header type 4',
      #         tagNames: ['h4'],
      #         contentDefault: '',
      #         classList: ['subtitle']
      #       }
      #     ]
      #   }
      # })
      # editor.subscribe 'editableInput', (event) ->
      #   textarea.val(editable.innerHTML)#.trigger 'change'
      ed.addClass('medium-editor-initiated')

  $(document).on 'nested:fieldAdded', "#drag-content-blocks", (e)->
    window.railsAdminContentBlocks.recalculateBlockOrders()
